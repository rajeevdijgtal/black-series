$(".carousel").on("touchstart", function(e) {
    var t = e.originalEvent.touches[0].pageX;
    $(this).one("touchmove", function(e) {
        var o = e.originalEvent.touches[0].pageX;
        Math.floor(t - o) > 5 ? $(this).carousel("next") : Math.floor(t - o) < -5 && $(this).carousel("prev")
    }), $(".carousel").on("touchend", function() {
        $(this).off("touchmove")
    })
}), $("#myCarousel").on("slide.bs.carousel", function(e) {
    var t = $(e.relatedTarget).index(),
        o = $(".card-carousel-item").length;
    if (t >= o - 2)
        for (var n = 3 - (o - t), r = 0; r < n; r++) "left" == e.direction ? $(".card-carousel-item").eq(r).appendTo(".card-carousel-inner") : $(".card-carousel-item").eq(0).appendTo($(this).find(".card-carousel-inner"))
}), $(document).ready(function() {
    $(window).resize(function() {
        $(window).width() >= 980 && ($(".navbar .dropdown-toggle").hover(function() {
            $(this).parent().toggleClass("show"), $(this).parent().find(".dropdown-menu").toggleClass("show")
        }), $(".navbar .dropdown-menu").mouseleave(function() {
            $(this).removeClass("show")
        }))
    })
}), "remove" in Element.prototype || (Element.prototype.remove = function() {
    this.parentNode && this.parentNode.removeChild(this)
}), mapboxgl.accessToken = "pk.eyJ1IjoianBhdGVsLWJzYyIsImEiOiJjamgxNXJmZ3EwNW5pMndtcndwY3pjZjVnIn0.Bg5o-cUHSzR7hnNFX-l2YQ";
var map = new mapboxgl.Map({
    container: "map",
    style: "mapbox://styles/mapbox/streets-v9",
    center: [-112.4512, 38],
    zoom: 4
});
map.addControl(new mapboxgl.NavigationControl);
var stores = {
    type: "FeatureCollection",
    features: [{
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [-118.029874, 34.0739874]
        },
        properties: {
            name: "Black Series Campers",
            phoneFormatted: "(626) 579-1069",
            phone: "6265791069",
            address: "19501 E Walnut Drive South",
            city: "City of Industry",
            state: "CA",
            postalCode: "91748",
            country: "United States"
        }
    }, {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [-111.12986, 45.6696]
        },
        properties: {
            name: "Big Sky RV",
            phoneFormatted: "(800) 877-9606",
            phone: "8008779606",
            address: "8466 Huffine Ln",
            city: "Bozeman",
            state: "MT",
            postalCode: "79718",
            country: "United States",
            website: "https://www.bigskyrv.com/"
        }
    }, {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [-104.89373, 39.77331]
        },
        properties: {
            name: "B & B RV",
            phoneFormatted: "(303) 322-6013",
            phone: "3033226013",
            address: "8101 e.40th ave",
            city: "Denver",
            state: "CO",
            postalCode: "80207",
            country: "United States",
            website: "https://www.bbrv.me/"
        }
    }, {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [-86.955048, 34.628262]
        },
        properties: {
            name: "Rocket City RV",
            phoneFormatted: "(256) 724-2267",
            phone: "2567242267",
            address: "21768 Alabama Highway 20",
            city: "Tanner",
            state: "AL",
            postalCode: "35671",
            country: "United States",
            website: "https://www.rocketcityrv.com/"
        }
    }, {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [-84.16729, 33.93316]
        },
        properties: {
            name: "Southland RV",
            phoneFormatted: "(770) 717-2890",
            phone: "7707172890",
            address: "1794 Bolton Cir",
            city: "Norcross",
            state: "GA",
            postalCode: "30071",
            country: "United States",
            website: "https://www.southlandrv.com/"
        }
    }, {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [-123.12976, 44.05515]
        },
        properties: {
            name: "Sutton RV",
            phoneFormatted: "(541) 686-6296",
            phone: "5416866296",
            address: "2400 W 7th Ave",
            city: "Eugene",
            state: "OR",
            postalCode: "97402",
            country: "United States"
        }
    }, {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [-91.151225, 42.15619]
        },
        properties: {
            name: "Iowa Showroom",
            phoneFormatted: "(319) 465-3021",
            phone: "3194653021",
            address: "15403 140th Ave",
            city: "Scotch Grove",
            state: "IA",
            postalCode: "52310",
            country: "United States"
        }
    }, {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [-117.988605, 33.742638]
        },
        properties: {
            name: "Beach Cities RV",
            phoneFormatted: "(714) 892-4202",
            phone: "7148924202",
            address: "15132 Beach BLVD",
            city: "Midway City",
            state: "CA",
            postalCode: "92655",
            country: "United States"
        }
    }, {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [-111.887573, 33.4089]
        },
        properties: {
            name: "Arizona Showroom",
            phoneFormatted: "(480) 280-8080",
            phone: "4802808080",
            address: "1137 W Birchwood Ave",
            city: "Mesa",
            state: "AZ",
            postalCode: "85210",
            country: "United States"
        }
    }, {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [-111.093731, 39.32098]
        },
        properties: {
            name: "RV’s of America",
            phoneFormatted: "(801) 860-0035",
            phone: "8018600035",
            address: "970 E. State Road",
            city: "America Fork",
            state: "UT",
            postalCode: "84003",
            country: "United States"
        }
    }, {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [-91.829411, 46.100699]
        },
        properties: {
            name: "Link RV",
            phoneFormatted: "(844) 395-4647",
            phone: "8443954647",
            address: "2700 Decker Dr",
            city: "Rice Lake",
            state: "WI",
            postalCode: "54868",
            country: "United States"
        }
    }, {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [-111.85859, 33.40665]
        },
        properties: {
            name: "Tom's Camperland",
            phoneFormatted: "(480) 894-1267",
            phone: "4808941267",
            address: "1301 W. Broadway",
            city: "Mesa",
            state: "AZ",
            postalCode: "85202",
            country: "United States",
            website: "https://www.tomscamperland.com/"
        }
    }, {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [-97.346953, 32.37211]
        },
        properties: {
            name: "Fun Town RV - Cleburne",
            phoneFormatted: "(855) 867-1433",
            phone: "8558671433",
            address: "2200 US 67 East Business",
            city: "Cleburne",
            state: "TX",
            postalCode: "76031",
            country: "United States",
            website: "https://www.funtownrv.com/"
        }
    }, {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [-96.461305, 32.899705]
        },
        properties: {
            name: "Fun Town RV - Dallas",
            phoneFormatted: "(855) 925-2760",
            phone: "8559252760",
            address: "2315 E INTERSTATE 30",
            city: "Rockwall",
            state: "TX",
            postalCode: "75087-6218",
            country: "United States",
            website: "https://www.funtownrv.com/"
        }
    }, {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [-97.17958, 33.274032]
        },
        properties: {
            name: "Fun Town RV - Denton",
            phoneFormatted: "(855) 475-4658",
            phone: "8554754658",
            address: "7201 Interstate 35",
            city: "Denton",
            state: "TX",
            postalCode: "76201",
            country: "United States",
            website: "https://www.funtownrv.com/"
        }
    }, {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [-96.956788, 30.185281]
        },
        properties: {
            name: "Fun Town RV - Giddings",
            phoneFormatted: "(844) 734-1827",
            phone: "8447341827",
            address: "1906 West Austin Street",
            city: "Giddings",
            state: "TX",
            postalCode: "78942",
            country: "United States",
            website: "https://www.funtownrv.com/"
        }
    }, {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [-96.089621, 29.365862]
        },
        properties: {
            name: "Fun Town RV - Houston",
            phoneFormatted: "(844) 242-5100",
            phone: "8442425100",
            address: "6767 US Hwy 59",
            city: "Wharton",
            state: "TX",
            postalCode: "77488",
            country: "United States",
            website: "https://www.funtownrv.com/"
        }
    }, {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [-100.376264, 31.493098]
        },
        properties: {
            name: "Fun Town RV - San Angelo",
            phoneFormatted: "(855) 395-1819",
            phone: "8553951819",
            address: "3560 North U.S. Hwy 67",
            city: "San Angelo",
            state: "TX",
            postalCode: "76905",
            country: "United States",
            website: "https://www.funtownrv.com/"
        }
    }, {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [-97.189645, 31.432284]
        },
        properties: {
            name: "Fun Town RV - Waco",
            phoneFormatted: "(855) 395-1861",
            phone: "8553951861",
            address: "777 Enterprise Blvd.",
            city: "Hewitt",
            state: "TX",
            postalCode: "76643",
            country: "United States",
            website: "https://www.funtownrv.com/"
        }
    }, {
        type: "Feature",
        geometry: {
            type: "Point",
            coordinates: [-97.406918, 35.081185]
        },
        properties: {
            name: "Fun Town RV - Oklahoma",
            phoneFormatted: "(855) 996-5526",
            phone: "8559965526",
            address: "25513 Johnson Ave",
            city: "Purcell",
            state: "OK",
            postalCode: "73080",
            country: "United States",
            website: "https://www.funtownrv.com/"
        }
    }]
};

function flyToStore(e) {
    map.flyTo({
        center: e.geometry.coordinates,
        zoom: 15
    })
}

function createPopUp(e) {
    var t = document.getElementsByClassName("mapboxgl-popup");
    t[0] && t[0].remove();
    new mapboxgl.Popup({
        closeOnClick: !1
    }).setLngLat(e.geometry.coordinates).setHTML("<h3>Black Series</h3><h4>" + e.properties.name + "</h4>").addTo(map)
}

function buildLocationList(e) {
    for (i = 0; i < e.features.length; i++) {
        var t = e.features[i].properties,
            o = document.getElementById("listings").appendChild(document.createElement("div"));
        o.className = "item", o.id = "listing-" + i;
        var n = o.appendChild(document.createElement("a"));
        n.href = "#", n.className = "title", n.dataPosition = i, n.innerHTML = t.name, o.appendChild(document.createElement("div")).innerHTML = t.address;
        var r = o.appendChild(document.createElement("div"));
        if (r.innerHTML = t.city, t.state && (r.innerHTML += " , " + t.state), t.postalCode && (r.innerHTML += " - " + t.postalCode), o.appendChild(document.createElement("div")).innerHTML = t.phoneFormatted, t.website) {
            var a = o.appendChild(document.createElement("a"));
            a.setAttribute("href", t.website), a.innerHTML = t.website, a.target = "_blank", a.style.color = "#333"
        }
        if (t.distance) {
            var s = Math.round(100 * t.distance) / 100;
            r.innerHTML += "<p><strong>" + s + " miles away</strong></p>"
        }
        n.addEventListener("click", function(t) {
            var o = e.features[this.dataPosition];
            flyToStore(o), createPopUp(o);
            var n = document.getElementsByClassName("active");
            n[0] && n[0].classList.remove("active"), this.parentNode.classList.add("active")
        })
    }
}

function validateEmail(e) {
    return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(e)
}

function subscribe() {
    validate() && $.ajax({
        url: "https://zmh091ll0k.execute-api.us-east-1.amazonaws.com/prod/Newsletter",
        method: "POST",
        crossDomain: !0,
        data: JSON.stringify({
            email: $(".newsletter-email").val()
        }),
        success: function(e) {
            var t = JSON.parse(e);
            console.log(t.status)
        },
        error: function(e, t) {
            console.log("error")
        }
    })
}

function validate() {
    var e = $("#result"),
        t = $(".newsletter-email").val();
    return e.text(""), validateEmail(t) ? (e.text("Thank you for your subscription!"), e.css({
        color: "#218838",
        "border-style": "solid",
        "border-width": "1px",
        "border-radius": "3px",
        "border-color": "#28a745"
    }), !0) : (e.text(t + " is not valid!"), e.css({
        color: "#FF595E",
        "border-style": "solid",
        "border-width": "1px",
        "border-radius": "3px",
        "border-color": "#dc3545"
    }), !1)
}

function contactFormSubmit() {
    if (!(contactRequiredFieldValidate() & contactEmailValidate() & contactPhoneValidate())) return console.log("error"), !1;
    var e = $("#message");
    e.text("Thank you for submitting your message! One of our representative will reach out soon."), e.css({
        color: "#218838",
        "border-style": "solid",
        "border-width": "1px",
        "border-radius": "3px",
        "border-color": "#28a745",
        margin: "8% auto",
        padding: "1%"
    }), $.ajax({
        url: "https://2zyzi238u4.execute-api.us-east-1.amazonaws.com/prod/ContactCRUD",
        method: "POST",
        crossDomain: !0,
        data: JSON.stringify({
            customer_name: $("#name").val(),
            customer_email: $("#email").val(),
            customer_phone: $("#phone").val(),
            customer_message: $("#contactMessage").val()
        }),
        success: function(e) {
            JSON.parse(e);
            console.log(JSON.stringify({
                customer_name: $("#name").val(),
                customer_email: $("#email").val(),
                customer_phone: $("#phone").val(),
                customer_message: $("#contactMessage").val()
            }))
        },
        error: function(e, t) {
            console.log(JSON.stringify({
                customer_name: $("#name").val(),
                customer_email: $("#email").val(),
                customer_phone: $("#phone").val(),
                customer_message: $("#contactMessage").val()
            }))
        }
    }), document.getElementById("contactForm").reset()
}

function contactEmailValidate() {
    var e = $("#result1"),
        t = $("#email").val();
    if (e.text(""), validateEmail(t)) return !0;
    if ("" === $("#email").val()) return !1;
    return e.text(t + " is not valid!"), e.css({
        color: "#FF595E"
    }), document.getElementById("email").onkeyup = function() {
        document.getElementById("result1").innerHTML = ""
    }, !1
}

function validatePhoneNumber(e) {
    return /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/.test(e)
}

function contactPhoneValidate() {
    var e = $("#result2"),
        t = $("#phone").val();
    if (e.text(""), validatePhoneNumber(t)) return !0;
    if ("" === $("#phone").val()) return !1;
    return e.text(t + " is not valid!"), e.css({
        color: "#FF595E"
    }), document.getElementById("phone").onkeyup = function() {
        document.getElementById("result2").innerHTML = ""
    }, !1
}

function contactRequiredFieldValidate() {
    if ("" === $("#name").val() || "" === $("#email").val() || "" === $("#phone").val() || "" === $("#contactMessage").val()) {
        function e() {
            document.getElementById(this.id + "_error").innerHTML = ""
        }
        return "" === $("#name").val() && (nameError = "Please enter your name", document.getElementById("name_error").innerHTML = nameError), "" === $("#email").val() && (emailError = "Please enter your email", document.getElementById("email_error").innerHTML = emailError), "" === $("#phone").val() && (telephoneError = "Please enter your phone", document.getElementById("phone_error").innerHTML = telephoneError), "" === $("#contactMessage").val() && (contactMessageError = "Please enter your message", document.getElementById("contactMessage_error").innerHTML = contactMessageError), document.getElementById("name").onkeyup = e, document.getElementById("email").onkeyup = e, document.getElementById("phone").onkeyup = e, document.getElementById("contactMessage").onkeyup = e, !1
    }
    return !0
}

function quoteFormSubmit() {
    if (!(quoteRequiredFieldValidate() & contactEmailValidate() & contactPhoneValidate())) return console.log("error"), !1;
    var e = $("#message");
    e.text("Thank you for submitting your message! One of our representative will reach out soon."), e.css({
        color: "#218838",
        "border-style": "solid",
        "border-width": "1px",
        "border-radius": "3px",
        "border-color": "#28a745",
        margin: "8% auto",
        padding: "1%"
    }), $.ajax({
        url: "https://ai7h0zgy9a.execute-api.us-east-1.amazonaws.com/prod/QuoteCRUD",
        method: "POST",
        crossDomain: !0,
        data: JSON.stringify({
            customer_name: $("#name").val(),
            customer_email: $("#email").val(),
            customer_phone: $("#phone").val(),
            customer_time: $("#time").val(),
            customer_product: $("#interestedProduct").val(),
            customer_message: $("#information").val()
        }),
        success: function(e) {
            JSON.parse(e);
            console.log(JSON.stringify({
                customer_name: $("#name").val(),
                customer_email: $("#email").val(),
                customer_phone: $("#phone").val(),
                customer_time: $("#time").val(),
                customer_product: $("#interestedProduct").val(),
                customer_message: $("#information").val()
            }))
        },
        error: function(e, t) {
            console.log(JSON.stringify({
                customer_name: $("#name").val(),
                customer_email: $("#email").val(),
                customer_phone: $("#phone").val(),
                customer_time: $("#time").val(),
                customer_product: $("#interestedProduct").val(),
                customer_message: $("#information").val()
            }))
        }
    }), document.getElementById("quoteForm").reset()
}

function quoteRequiredFieldValidate() {
    if ("" === $("#name").val() || "" === $("#email").val() || "" === $("#phone").val() || "" === $("#time").val() || "" === $("#interestedProduct").val()) {
        function e() {
            document.getElementById(this.id + "_error").innerHTML = ""
        }
        return "" === $("#name").val() && (nameError = "Please enter your name", document.getElementById("name_error").innerHTML = nameError), "" === $("#email").val() && (emailError = "Please enter your email", document.getElementById("email_error").innerHTML = emailError), "" === $("#phone").val() && (telephoneError = "Please enter your phone", document.getElementById("phone_error").innerHTML = telephoneError), "" === $("#time").val() && (timeError = "Please select estimated time", document.getElementById("time_error").innerHTML = timeError), "" === $("#interestedProduct").val() && (interestedProductError = "Please select your choice of product", document.getElementById("interestedProduct_error").innerHTML = interestedProductError), document.getElementById("name").onkeyup = e, document.getElementById("email").onkeyup = e, document.getElementById("phone").onkeyup = e, document.getElementById("time").onchange = e, document.getElementById("interestedProduct").onchange = e, !1
    }
    return !0
}
map.on("load", function(e) {
    map.addSource("places", {
        type: "geojson",
        data: stores
    }), buildLocationList(stores);
    var t = new MapboxGeocoder({
        accessToken: mapboxgl.accessToken,
        country: "us"
    });
    map.addControl(t, "top-left"), map.addSource("single-point", {
        type: "geojson",
        data: {
            type: "FeatureCollection",
            features: []
        }
    }), map.addLayer({
        id: "point",
        source: "single-point",
        type: "circle",
        paint: {
            "circle-radius": 10,
            "circle-color": "#007cbf",
            "circle-stroke-width": 3,
            "circle-stroke-color": "#fff"
        }
    }), t.on("result", function(e) {
        var t = e.result.geometry;
        map.getSource("single-point").setData(t);
        var o = {
            units: "miles"
        };
        stores.features.forEach(function(e) {
            Object.defineProperty(e.properties, "distance", {
                value: turf.distance(t, e.geometry, o),
                writable: !0,
                enumerable: !0,
                configurable: !0
            })
        }), stores.features.sort(function(e, t) {
            return e.properties.distance > t.properties.distance ? 1 : e.properties.distance < t.properties.distance ? -1 : 0
        });
        for (var n, r, a, s, i = document.getElementById("listings"); i.firstChild;) i.removeChild(i.firstChild);
        buildLocationList(stores), n = 0, r = [stores.features[n].geometry.coordinates[1], t.coordinates[1]], a = [stores.features[n].geometry.coordinates[0], t.coordinates[0]].sort(function(e, t) {
            return e > t ? 1 : e.distance < t.distance ? -1 : 0
        }), s = r.sort(function(e, t) {
            return e > t ? 1 : e.distance < t.distance ? -1 : 0
        }), map.fitBounds([
            [a[0], s[0]],
            [a[1], s[1]]
        ], {
            padding: 100
        }), createPopUp(stores.features[0])
    })
}), stores.features.forEach(function(e, t) {
    var o = document.createElement("div");
    o.id = "marker-" + t, o.className = "marker", new mapboxgl.Marker(o, {}).setLngLat(e.geometry.coordinates).addTo(map), o.addEventListener("click", function(o) {
        flyToStore(e), createPopUp(e);
        var n = document.getElementsByClassName("active");
        o.stopPropagation(), n[0] && n[0].classList.remove("active"), document.getElementById("listing-" + t).classList.add("active")
    })
}), $("#dominator-carousel").carousel({
    interval: 6e6
}), jQuery(document).ready(function(e) {
    var t = e(".cd-timeline-block");
    t.each(function() {
        e(this).offset().top > e(window).scrollTop() + .75 * e(window).height() && e(this).find(".cd-timeline-img, .cd-timeline-content").addClass("is-hidden")
    }), e(window).on("scroll", function() {
        t.each(function() {
            e(this).offset().top <= e(window).scrollTop() + .75 * e(window).height() && e(this).find(".cd-timeline-img").hasClass("is-hidden") && e(this).find(".cd-timeline-img, .cd-timeline-content").removeClass("is-hidden").addClass("bounce-in")
        })
    })
}), $("#myCarousel").on("slide.bs.carousel", function(e) {
    var t = $(e.relatedTarget).index(),
        o = $(".card-carousel-item").length;
    if (t >= o - 2)
        for (var n = 3 - (o - t), r = 0; r < n; r++) "left" == e.direction ? $(".card-carousel-item").eq(r).appendTo(".card-carousel-inner") : $(".card-carousel-item").eq(0).appendTo($(this).find(".card-carousel-inner"))
}), $(".subscribe-btn").bind("click", validate);